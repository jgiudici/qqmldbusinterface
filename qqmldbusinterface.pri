QQMLDBUSINTERFACE_INCLUDEPATH += $${PWD}/src
QQMLDBUSINTERFACE_LIBS += -L$${OUT_PWD}/$${DEPTH}/src
unix:QQMLDBUSINTERFACE_LIBS += -lqqmldbusinterface
win32:QQMLDBUSINTERFACE_LIBS += -lqqmldbusinterface1

QT += xml dbus quick

isEmpty(QQMLDBUSINTERFACE_LIBRARY_TYPE) {
    QQMLDBUSINTERFACE_LIBRARY_TYPE = shared
}

contains(QQMLDBUSINTERFACE_LIBRARY_TYPE, staticlib) {
    DEFINES += QQMLDBUSINTERFACE_STATIC
} else {
    DEFINES += QQMLDBUSINTERFACE_SHARED
}

isEmpty(PREFIX) {
    unix {
        PREFIX = /usr
    } else {
        PREFIX = $$[QT_INSTALL_PREFIX]
    }
}
                                                                     
isEmpty(LIBDIR) {
    LIBDIR = lib
}
