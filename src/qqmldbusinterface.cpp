#include <QJSValue>
#include <QJSEngine>
#include <QQmlContext>
#include <QQmlEngine>
#include <QMetaProperty>
#include <QDBusInterface>
#include <QDBusReply>
#include <QDBusMetaType>
#include <QTimer>
#include <QLoggingCategory>

#include "qqmldbusinterface_p.h"
#include "qqmldbusinterface.h"

// from dbus headers
#define DBUS_INTERFACE_DBUS           "org.freedesktop.DBus"
#define DBUS_INTERFACE_INTROSPECTABLE "org.freedesktop.DBus.Introspectable"
#define DBUS_INTERFACE_PROPERTIES     "org.freedesktop.DBus.Properties"

Q_LOGGING_CATEGORY(QQMLDBUSINTERFACE_LOGGING, "qqmldbusinterface.logging")

QQmlDBusInterfaceMetaObject::QQmlDBusInterfaceMetaObject(QQmlDBusInterface *qq,
                                                         QQmlDBusInterfacePrivate *dd,
                                                         const QMetaObject *mo)
    : q(qq),
      d(dd)
{
    m_builder.setSuperClass(mo);
    m_builder.setClassName(mo->className());
    m_builder.setFlags(QMetaObjectBuilder::DynamicMetaObject);

    m_metaObject = m_builder.toMetaObject();
    m_propertyOffset = m_metaObject->propertyOffset();
    m_methodOffset = m_metaObject->methodOffset();

    // tricky
    QObjectPrivate *op = QObjectPrivate::get(q);
    m_parent = static_cast<QAbstractDynamicMetaObject*>(op->metaObject);
    *static_cast<QMetaObject*>(this) = *m_metaObject;
    op->metaObject = this;
}

QQmlDBusInterfaceMetaObject::~QQmlDBusInterfaceMetaObject()
{
    free(m_metaObject);
}

void QQmlDBusInterfaceMetaObject::updateProperties(const QVariantMap &data)
{
    // NOTE: eventually use QQmlPropertyCache here
    foreach (QString key, data.keys()) {
        int propertyIndex = m_propertyIdLookup.value(key.toUtf8());
        activate(q, propertyIndex + m_methodOffset, 0);
    }
}

void QQmlDBusInterfaceMetaObject::dispatchSignal(const QDBusMessage &message)
{
    qCDebug(QQMLDBUSINTERFACE_LOGGING) << Q_FUNC_INFO << message;
    // NOTE: this is _very_ rudimentary right now, we can probably make it crash
    //       easily. Also it doesn't account for overloads, etc.

    if (!m_signalIdLookup.contains(message.member().toUtf8())) {
        qCDebug(QQMLDBUSINTERFACE_LOGGING) << "\tinvalid signal: " << message.member();
    }
    int signalIndex = m_signalIdLookup.value(message.member().toUtf8());
    qCDebug(QQMLDBUSINTERFACE_LOGGING) << "emitting signal: " << signalIndex + m_methodOffset - 1;

    QVarLengthArray<void *, 10> argv;
    QVarLengthArray<QVariant, 10> scope;
    argv.reserve(message.arguments().size() + 1);
    argv.append(0);
    for (int i = 0; i < message.arguments().size(); ++i) {
        const QVariant &variant = QQmlDBusInterfacePrivate::extractValueFromMessage(message, i);
        scope.append(variant);
        argv.append(const_cast<void *>(variant.constData()));
    }

    activate(q, signalIndex + m_methodOffset - 1, argv.data());
}

void QQmlDBusInterfaceMetaObject::initFromInterface(const QDBusIntrospection::Interface *iface)
{
    foreach (const QDBusIntrospection::Property property, iface->properties.values())
        addProperty(property);
    QList<MetaSignalInfo> metaSignals;
    foreach (const QDBusIntrospection::Signal signal, iface->signals_.values()) {
        if (signal.name == QLatin1String("PropertiesChanged") ||
            signal.name.isEmpty())
            continue;
        metaSignals << addSignal(signal);
    }
    foreach (const QDBusIntrospection::Method method, iface->methods.values())
        addMethod(method);

    // replace the old metaObject
    free(m_metaObject);
    m_metaObject = m_builder.toMetaObject();
    *static_cast<QMetaObject*>(this) = *m_metaObject;

    // now connect signals
     foreach (MetaSignalInfo metaSignal, metaSignals) {
        if (!d->connection.connect(d->service, d->path, d->interface, metaSignal.name,
                                   q, SLOT(_q_forwardSignal(QDBusMessage)))) {
            qCDebug(QQMLDBUSINTERFACE_LOGGING) << "couldn't connect signal(" << metaSignal.name << "), with signature: " << metaSignal.signature;
        }
    }
}

QByteArray QQmlDBusInterfaceMetaObject::dbusTypeName(const QString &dbusType)
{
    return dbusTypeName(dbusType.toLatin1());
}

QByteArray QQmlDBusInterfaceMetaObject::dbusTypeName(const QByteArray &dbusType)
{
    int type = QDBusMetaType::signatureToType(dbusType);
    if (type == QMetaType::UnknownType) {
        if (dbusType == "a{sv}")
            return QMetaType::typeName(qMetaTypeId<QVariantMap>());
    }

    return QMetaType::typeName(type);
}

QMetaType::Type QQmlDBusInterfaceMetaObject::dbusType(const QByteArray &dbusType)
{
    int type = QDBusMetaType::signatureToType(dbusType);
    if (type == QMetaType::UnknownType) {
        if (dbusType == "a{sv}")
            type = qMetaTypeId<QVariantMap>();
    }

    return static_cast<QMetaType::Type>(type);
}

void QQmlDBusInterfaceMetaObject::addProperty(const QDBusIntrospection::Property property)
{
    int id = m_builder.propertyCount();
    m_builder.addSignal("__" + QByteArray::number(id) + "()");
    QByteArray propertyName = property.name.toUtf8();
    QMetaPropertyBuilder propertyBuilder =
        m_builder.addProperty(propertyName, "QVariant", id);
    bool readable = (property.access == QDBusIntrospection::Property::Read ||
                     property.access == QDBusIntrospection::Property::ReadWrite);
    bool writable = (property.access == QDBusIntrospection::Property::Write ||
                     property.access == QDBusIntrospection::Property::ReadWrite);
    propertyBuilder.setReadable(readable);
    propertyBuilder.setWritable(writable);
    m_propertyIdLookup.insert(propertyName, id);
    QMetaType::Type propertyType = dbusType(property.type.toLatin1());
    m_propertyTypeLookup.insert(propertyName, propertyType);
    qCDebug(QQMLDBUSINTERFACE_LOGGING) << "adding property: " << propertyName;
}

QQmlDBusInterfaceMetaObject::MetaSignalInfo
QQmlDBusInterfaceMetaObject::addSignal(const QDBusIntrospection::Signal &signal)
{
    QList<QByteArray> parameterNames;
    QByteArray signature;
    foreach (const QDBusIntrospection::Argument arg, signal.outputArgs) {
        QByteArray type = dbusTypeName(arg.type);
        signature += type + ",";
        parameterNames += arg.name.toLatin1();
    }
    signature.chop(1);

    QByteArray signalSignature = signal.name.toLatin1() + "(" + signature + ")";
    if (m_builder.indexOfSignal(signalSignature) > -1) {
        qWarning() << Q_FUNC_INFO << "signal already exists: " << signalSignature;
        return MetaSignalInfo();
    }

    QMetaMethodBuilder metaSignal = m_builder.addSignal(signalSignature);
    metaSignal.setParameterNames(parameterNames);
    qCDebug(QQMLDBUSINTERFACE_LOGGING) << "adding signal: " << signalSignature;

    MetaSignalInfo info;
    info.name = signal.name;
    info.signature = signalSignature;
    info.parameterNames = parameterNames;

    m_signalIdLookup.insert(signal.name.toUtf8(), m_builder.methodCount());
    return info;
}

void QQmlDBusInterfaceMetaObject::addMethod(const QDBusIntrospection::Method &method)
{
    QList<QByteArray> parameterNames;
    QByteArray signature;
    foreach (const QDBusIntrospection::Argument arg, method.inputArgs) {
        QByteArray type = dbusTypeName(arg.type);
        signature += type + ",";
        parameterNames += arg.name.toLatin1();
    }

    // add a callback
    signature += "QJSValue";

    QByteArray returnType = "void";
    if (!method.outputArgs.isEmpty()) {
        const QDBusIntrospection::Argument arg = method.outputArgs.first();
        returnType = dbusTypeName(arg.type);

        if (returnType == "QDBusVariant")
            returnType = "QVariant";
    }

    QByteArray methodSignature = method.name.toLatin1() + "(" + signature + ")";
    QMetaMethodBuilder metaMethod =
        m_builder.addMethod(methodSignature, returnType);
    metaMethod.setParameterNames(parameterNames);
    qCDebug(QQMLDBUSINTERFACE_LOGGING) << "adding method: " << methodSignature << " , returnType: " << returnType;
}

int QQmlDBusInterfaceMetaObject::metaCall(QMetaObject::Call _c, int _id, void **_a)
{
    if ((_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty) &&
        _id >= m_propertyOffset) {
        int propertyId = _id - m_propertyOffset;
        QByteArray propertyName = m_propertyIdLookup.key(propertyId);
        if (_c == QMetaObject::ReadProperty)
            *reinterpret_cast<QVariant*>(_a[0]) = d->property(propertyName);
        else {
            int propertyType = m_propertyTypeLookup.value(propertyName);
            QVariant value = *reinterpret_cast<QVariant*>(_a[0]);
            if (value.canConvert(propertyType))
                value.convert(propertyType);

            d->setProperty(propertyName, value);
        }

        return -1;
    } else if (_c == QMetaObject::InvokeMetaMethod) {

        if (_id >= m_methodOffset) {
            QMetaMethod mm = m_metaObject->method(_id);
            qCDebug(QQMLDBUSINTERFACE_LOGGING) << "attempting to call method: " << mm.name();

            QVariantList args;
            for (int i = 0; i < mm.parameterCount() - 1; ++i) {
                QVariant variant(mm.parameterType(i), _a[i + 1]);
                qCDebug(QQMLDBUSINTERFACE_LOGGING) << "\t" << variant;
                args << variant;
            }

            QJSValue callback = *reinterpret_cast<QJSValue*>(_a[mm.parameterCount()]);
            if (!callback.isCallable()) {
                qCDebug(QQMLDBUSINTERFACE_LOGGING) << Q_FUNC_INFO << "invalid callback specified";
                return -1;
            }

            QDBusMessage msg =
                QDBusMessage::createMethodCall(d->service, d->path, d->interface, mm.name());
            msg.setArguments(args);
            QDBusMessagePrivate::setParametersValidated(msg, true);
            QDBusPendingCall reply = d->connection.asyncCall(msg, d->timeout);
            QDBusPendingCallWatcher *watcher = new QDBusPendingCallWatcher(reply, q);
            QObject::connect(watcher, SIGNAL(finished(QDBusPendingCallWatcher*)),
                                   q, SLOT(_q_pendingWatcherFinished(QDBusPendingCallWatcher*)));
            d->pendingCalls.insert(watcher, callback);
        } else {
            qCDebug(QQMLDBUSINTERFACE_LOGGING) << "attempting to call method with invalid id: " << _id;
        }
    }

    if (m_parent)
        return m_parent->metaCall(_c, _id, _a);
    return q->qt_metacall(_c, _id, _a);
}

int QQmlDBusInterfaceMetaObject::createProperty(const char *name, const char *)
{
    qCDebug(QQMLDBUSINTERFACE_LOGGING) << Q_FUNC_INFO << "name: " << name;
    return -1;
}

QQmlDBusInterfacePrivate::QQmlDBusInterfacePrivate(const QString &s, const QString &p,
                         const QString &i, const QDBusConnection &c,
                         QQmlDBusInterface *iface)
    : q(iface),
      connection(c),
      service(s),
      path(p),
      interface(i),
      timeout(-1),
      valid(false)
{
    if (!connection.isConnected()) {
        lastError = QDBusError(QDBusError::Disconnected,
                               QStringLiteral("Not connected to D-Bus server"));
    }
}

QQmlDBusInterface::QQmlDBusInterface(const QString &service, const QString &path,
                                     const QString &interface,
                                     const QDBusConnection &connection, QObject *parent)
    : QObject(*new QQmlDBusInterfacePrivate(service, path, interface, connection, this), parent)
{
    Q_D(QQmlDBusInterface);
    if (qgetenv("QQMLDBUSINTERFACE_DEBUG").toInt())
        QQMLDBUSINTERFACE_LOGGING().setEnabled(QtDebugMsg, true);
    d->metaObject = new QQmlDBusInterfaceMetaObject(this, d, metaObject());
    d->configureInterface();
}

bool QQmlDBusInterface::isValid() const
{
    Q_D(const QQmlDBusInterface);
    return d->valid;
}

void QQmlDBusInterfacePrivate::configureInterface()
{
    QString xml = introspect();
    QDBusIntrospection::Interfaces ifaces = QDBusIntrospection::parseInterfaces(xml);
    if (!ifaces.contains(interface)) {
        qCWarning(QQMLDBUSINTERFACE_LOGGING) << interface << "was not found during introspection."
                                             << "This interface may not function correctly.";
        return;
    }

    // first check if PropertiesChanged exists
    // bool propertiesChangedExists = false;
    const QDBusIntrospection::Interface *propertiesIface =
        ifaces.value(DBUS_INTERFACE_PROPERTIES);
    foreach (const QDBusIntrospection::Signal signal, propertiesIface->signals_.values()) {
        if (signal.name == QLatin1String("PropertiesChanged")) {
            connection.connect(service, path, interface, QLatin1String("PropertiesChanged"),
                q, SLOT(_q_processPropertiesChanged(QString,QVariantMap,QStringList)));
            break;
        }
    }

    // This needs to happen because we use QDBusConnection::registerObject() on our root object,
    // which recursively registers all of our modules. This generates an interface for every level
    // of inheritance in our module, so each of those interfaces must be identified if we want to
    // use our object as a whole. Otherwise, we would only be able to access members explicitly defined
    // in our module, and we would be unable to reach anything that is inherited.
    foreach (const QDBusIntrospection::Interface *iface, ifaces) {
        if (!iface->name.startsWith("org.freedesktop.DBus"))
            metaObject->initFromInterface(iface);
    }

    valid = true;
}

QQmlDBusInterface::~QQmlDBusInterface()
{
}

QString QQmlDBusInterfacePrivate::introspect()
{
    QDBusMessage msg = QDBusMessage::createMethodCall(service, path,
                              QStringLiteral(DBUS_INTERFACE_INTROSPECTABLE),
                              QStringLiteral("Introspect"));
    QDBusMessagePrivate::setParametersValidated(msg, true);
    QDBusMessage reply = connection.call(msg, QDBus::Block, timeout);
    if (reply.type() != QDBusMessage::ReplyMessage) {
        qCDebug(QQMLDBUSINTERFACE_LOGGING) << "failed to introspect: " << reply;
        return QString();
    }

    return QDBusReply<QString>(reply);
}

bool QQmlDBusInterfacePrivate::setProperty(const QByteArray &property, const QVariant &value)
{
//    if (!isValid || !canMakeCalls()) {   // can't make calls
//        where.clear();
//        return;
//    }

    QDBusMessage msg =
        QDBusMessage::createMethodCall(service, path,
                                       QStringLiteral(DBUS_INTERFACE_PROPERTIES),
                                       QStringLiteral("Set"));
    QDBusMessagePrivate::setParametersValidated(msg, true);
    msg << interface << QString::fromUtf8(property)
        << QVariant::fromValue(QDBusVariant(value));
    QDBusMessage reply = connection.call(msg, QDBus::Block, timeout);
    if (reply.type() != QDBusMessage::ReplyMessage) {
        lastError = QDBusError(reply);
        return false;
    }

    return true;
}

QVariant QQmlDBusInterfacePrivate::property(const QByteArray &property)
{
//    if (!isValid || !canMakeCalls()) {   // can't make calls
//        where.clear();
//        return;
//    }

    QDBusMessage msg = QDBusMessage::createMethodCall(service, path,
                                                      QStringLiteral(DBUS_INTERFACE_PROPERTIES),
                                                      QStringLiteral("Get"));
    QDBusMessagePrivate::setParametersValidated(msg, true);
    msg << interface << QString::fromUtf8(property);
    QDBusMessage reply = connection.call(msg, QDBus::Block, timeout);
    if (reply.type() != QDBusMessage::ReplyMessage) {
        lastError = QDBusError(reply);
        return QVariant();
    }

    if (reply.signature() != QStringLiteral("v")) {
        QString errmsg = QStringLiteral("Invalid signature `%1' in return from call to "
                                       DBUS_INTERFACE_PROPERTIES);
        lastError = QDBusError(QDBusError::InvalidSignature, errmsg.arg(reply.signature()));
        return QVariant();
    }

    QMetaType::Type propertyType = metaObject->m_propertyTypeLookup.value(property);
    QVariant result = extractValueFromMessage(reply);
    if (propertyType == qMetaTypeId<QVariantMap>()) {
        QVariantMap map = qdbus_cast<QVariantMap>(result);
        convertMessageValues(map);
        return map;
    } else if (result.userType() == qMetaTypeId<QDBusVariant>()) {
        return qdbus_cast<QVariant>(result);
    }

    return result;
}

void QQmlDBusInterfacePrivate::convertMessageValues(QVariantMap &map)
{
    foreach (QString key, map.keys()) {

        QVariant value = map.value(key);
        if (value.userType() == qMetaTypeId<QDBusVariant>()) {
            map.insert(key, value.value<QDBusVariant>().variant());
        } else if (value.userType() == qMetaTypeId<QDBusArgument>()) {
            QVariantMap subMap = qdbus_cast<QVariantMap>(value);
            convertMessageValues(subMap);
            map.insert(key, subMap);
        }
    }
}

QVariant QQmlDBusInterfacePrivate::extractValueFromMessage(const QDBusMessage &message, int idx)
{
    if (idx > message.arguments().size()) {
        qCDebug(QQMLDBUSINTERFACE_LOGGING) << Q_FUNC_INFO << "invalid index: " << idx;
        return QVariant();
    }

    QVariant returnValue = message.arguments().at(idx);
    if (returnValue.userType() == qMetaTypeId<QDBusVariant>()) {
        returnValue = returnValue.value<QDBusVariant>().variant();
    } else if (returnValue.userType() == qMetaTypeId<QDBusArgument>()) {
        if (message.signature() == "a{sv}") {
            QVariantMap map = qdbus_cast<QVariantMap>(returnValue);
            convertMessageValues(map);
            returnValue = map;
        }
    }

    return returnValue;
}

void QQmlDBusInterfacePrivate::_q_pendingWatcherFinished(QDBusPendingCallWatcher *watcher)
{
    if (!watcher) {
        qCDebug(QQMLDBUSINTERFACE_LOGGING) << Q_FUNC_INFO << "invalid watcher";
        return;
    }

    QDBusPendingCall response = *watcher;
    QJSValue callback = pendingCalls.take(watcher);
    watcher->deleteLater();

    QJSValueList args;
    if (response.isError()) {
        args.append(callback.engine()->toScriptValue(response.error().message()));
        args.append(QJSValue(QJSValue::NullValue));
    } else {
        args.append(QJSValue(QJSValue::NullValue));

        QVariant result;
        QDBusMessage message = response.reply();
        if (!message.arguments().isEmpty())
            result = extractValueFromMessage(message);
        args.append(callback.engine()->toScriptValue(result));
    }

    callback.call(args);
}

void QQmlDBusInterfacePrivate::_q_processPropertiesChanged(const QString &iface,
                                                           const QVariantMap &changedProperties,
                                                           const QStringList &invalidatedProperties)
{
    Q_UNUSED(invalidatedProperties)
    qCDebug(QQMLDBUSINTERFACE_LOGGING) << Q_FUNC_INFO;
    if (iface != interface) {
        qCDebug(QQMLDBUSINTERFACE_LOGGING) << "invalid interface: " << iface;
        return;
    }

    metaObject->updateProperties(changedProperties);
}

void QQmlDBusInterfacePrivate::_q_forwardSignal(const QDBusMessage &message)
{
    if (message.type() == QDBusMessage::SignalMessage)
        metaObject->dispatchSignal(message);
}

QString QQmlDBusInterface::service() const
{
    Q_D(const QQmlDBusInterface);
    return d->service;
}

void QQmlDBusInterface::setService(const QString &service)
{
    Q_D(QQmlDBusInterface);
    if (d->service == service)
        return;

    d->service = service;
    Q_EMIT serviceChanged();
}

QString QQmlDBusInterface::path() const
{
    Q_D(const QQmlDBusInterface);
    return d->path;
}

void QQmlDBusInterface::setPath(const QString &path)
{
    Q_D(QQmlDBusInterface);
    if (d->path == path)
        return;

    d->path = path;
    Q_EMIT pathChanged();
}

QString QQmlDBusInterface::interface() const
{
    Q_D(const QQmlDBusInterface);
    return d->interface;
}

void QQmlDBusInterface::setInterface(const QString &interface)
{
    Q_D(QQmlDBusInterface);
    if (d->interface == interface)
        return;

    d->interface = interface;
    Q_EMIT interfaceChanged();
}

QDBusConnection::BusType QQmlDBusInterface::busType() const
{
    Q_D(const QQmlDBusInterface);
    return d->busType;
}

void QQmlDBusInterface::setBusType(QDBusConnection::BusType busType)
{
    Q_D(QQmlDBusInterface);
    if (d->busType == busType)
        return;

    d->busType = busType;
    Q_EMIT busTypeChanged();
}

#include "moc_qqmldbusinterface.cpp"
