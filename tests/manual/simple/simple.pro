DEPTH = ../../..
include($${DEPTH}/qqmldbusinterface.pri)

TEMPLATE = app
TARGET = simple
INCLUDEPATH += $${QQMLDBUSINTERFACE_INCLUDEPATH}
LIBS += $${QQMLDBUSINTERFACE_LIBS}

SOURCES += main.cpp
