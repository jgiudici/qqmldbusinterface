#include <QDebug>
#include "emptyadaptorbase.h"

EmptyAdaptorBase::EmptyAdaptorBase(QObject *parent)
    : QObject(parent),
      m_emptyAdaptorBaseProp("Set By EmptyAdaptorBase")
{
    setObjectName("emptyadaptorbase");
}

void EmptyAdaptorBase::emptyAdaptorBaseSlot()
{
    qDebug() << Q_FUNC_INFO << "called!";
}

QString EmptyAdaptorBase::emptyAdaptorBaseProp() const
{
    return m_emptyAdaptorBaseProp;
}

void EmptyAdaptorBase::setEmptyAdaptorBaseProp(const QString &value)
{
    m_emptyAdaptorBaseProp = value;
}
