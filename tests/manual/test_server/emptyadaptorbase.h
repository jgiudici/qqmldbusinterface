#ifndef EMPTYADAPTORBASE_H
#define EMPTYADAPTORBASE_H

#include <QObject>

class EmptyAdaptorBase : public QObject
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "com.devonit.agent.settings.emptyadaptorbase")
    Q_PROPERTY(QString emptyAdaptorBaseProp READ emptyAdaptorBaseProp WRITE setEmptyAdaptorBaseProp NOTIFY emptyAdaptorBasePropChanged)

public:
    EmptyAdaptorBase(QObject *parent = 0);

    QString emptyAdaptorBaseProp() const;
    void setEmptyAdaptorBaseProp(const QString &value);

public Q_SLOTS:
    void emptyAdaptorBaseSlot();

Q_SIGNALS:
    void emptyAdaptorBasePropChanged();

private:
    QString m_emptyAdaptorBaseProp;

};

#endif // EMPTYADAPTOR_H
