import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Window 2.0
import QtQuick.Layouts 1.0
import QtQuick.Controls.Styles 1.0

ApplicationWindow {
    width: 700
    height: 700
    color: "transparent"
    visible: true

    GridLayout {
        columns: 1
        anchors.fill: parent
        width: parent.width
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter

        function convertVariantMapToText(map) {
            var text = "";
            for (var key in result)
                text += key + " = " + result[key] + "\n";
            return text;
        }

        // Database Properties
        Rectangle {
            id: dbProperties
            anchors.fill: parent
            color: "#eee"
            radius: 3.0

            Text {
                id: dnsTitle
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.topMargin: 4
                anchors.leftMargin: 4
                text: qsTr("Database Configuration")
                font.weight: Font.Bold
            }

            GridLayout {
                columns: 1
                anchors.top: dnsTitle.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.topMargin: 10
                anchors.bottomMargin: 4
                anchors.leftMargin: 10

                columnSpacing: 20
                rowSpacing: 4
                width: parent.width

                Text { text: qsTr("Username") }
                TextField {
                    id: username
                    placeholderText: qsTr("Username")
                    Layout.preferredWidth: 400
                    text: databaseProxy.username
                }

                Text { text: qsTr("Password") }
                TextField {
                    id: password
                    echoMode: TextInput.Password
                    Layout.preferredWidth: 400
                    text: databaseProxy.password
                }

                Text { text: qsTr("Database") }
                TextField {
                    id: name
                    placeholderText: qsTr("Database")
                    Layout.preferredWidth: 400
                    text: databaseProxy.name
                }

                Text { text: qsTr("Host IP") }
                TextField {
                    id: hostIP
                    placeholderText: qsTr("Host IP")
                    Layout.preferredWidth: 400
                    text: databaseProxy.hostIP
                }

                Text { text: qsTr("Host Port") }
                TextField {
                    id: hostPort
                    placeholderText: qsTr("Host Port")
                    Layout.preferredWidth: 400
                    validator: IntValidator { bottom: 1; top: 65535 }
                    text: databaseProxy.hostPort
                }

                Text { text: qsTr("Variant Property") }
                Row {
                    SpinBox {
                        id: variantSpinBox
                        value: databaseProxy.variantProperty
                    }
                    TextField {
                        id: variantField
                        text: databaseProxy.variantProperty
                    }
                }

                Text { text: qsTr("VariantMap Property") }
                TextArea {
                    id: variantMapTextArea
                    text: databaseProxy.variantMap
                }

                // XXX StringList property
                Text { text: qsTr("StringList Property") }
                Row {
                    TextField {
                        id: stringList1
                        text: databaseProxy.stringListProperty[0]
                    }
                    TextField {
                        id: stringList2
                        text: databaseProxy.stringListProperty[1]
                    }
                    TextField {
                        id: stringList3
                        text: databaseProxy.stringListProperty[2]
                    }
                    TextField {
                        id: stringList4
                        text: databaseProxy.stringListProperty[3]
                    }
                    TextField {
                        id: stringList5
                        text: databaseProxy.stringListProperty[4]
                    }
                }

                Column {
                    Row {
                        Button {
                            id: methodWithArguments
                            text: qsTr("methodWithArguments")
                            onClicked: {
                                databaseProxy.methodWithArguments("testing", 10, function(error, result) {
                                    if (error) {
                                        errorField.text = error;
                                        return;
                                    }

                                    resultField.text = result;
                                })
                            }
                        }

                        Button {
                            id: methodWithReturn
                            text: qsTr("methodWithReturn")
                            onClicked: {
                                databaseProxy.methodWithReturn(function(error, result) {
                                    if (error) {
                                        errorField.text = error;
                                        return;
                                    }

                                    resultField.text = result;
                                })
                            }
                        }

                        Button {
                            id: methodWithReturnAndArguments
                            text: qsTr("methodWithReturnAndArguments")
                            onClicked: {
                                databaseProxy.methodWithReturnAndArguments("hello", function(error, result) {
                                    if (error) {
                                        errorField.text = error;
                                        return;
                                    }

                                    resultField.text = result;
                                })
                            }
                        }

                        Button {
                            id: updateStringListProperty
                            text: qsTr("updateStringListProperty")
                            onClicked: {
                                databaseProxy.stringListProperty = [stringList1.text, stringList2.text, stringList3.text, stringList4.text, stringList5.text]
                                resultField.text = databaseProxy.stringListProperty[0] + " " + databaseProxy.stringListProperty[1] +
                                    " " + databaseProxy.stringListProperty[2] + " " + databaseProxy.stringListProperty[3] + " " + databaseProxy.stringListProperty[4]
                            }
                        }
                    }

                    Row {

                        Button {
                            id: methodThatReturnsAVariant
                            text: qsTr("methodThatReturnsAVariant")
                            onClicked: {
                                databaseProxy.methodThatReturnsAVariant(function(error, result) {
                                    if (error) {
                                        errorField.text = error;
                                        return;
                                    }

                                    resultField.text = result;
                                })
                            }
                        }

                        Button {
                            id: methodThatReturnsAVariantMap
                            text: qsTr("methodThatReturnsAVariantMap")
                            onClicked: {
                                databaseProxy.methodThatReturnsAVariantMap(function(error, result) {
                                    if (error) {
                                        errorField.text = error;
                                        return;
                                    }

                                    var text = "";
                                    for (var key in result) {
                                        text += key + " = " + result[key] + "\n";
                                    }
                                    resultField.text = text;
                                })
                            }
                        }

                        Button {
                            id: methodThatSendsAndReturnsAVariantMap
                            text: qsTr("methodThatTakesAndReturnsAVariantMap")
                            onClicked: {
                                var testData = {
                                    dogs: "rule",
                                    cats: "drool"
                                }

                                databaseProxy.methodThatTakesAndReturnsAVariantMap(testData, function(error, result) {
                                    if (error) {
                                        errorField.text = error;
                                        return;
                                    }

                                    resultField.text = convertVariantMapToText(result);
                                })
                            }
                        }

                    }

                    Row {
                        Button {
                            id: testSignalWithNoValue
                            text: "signalWithNoValue"
                            onClicked: databaseProxy.testSignalWithNoValue(function(error, result) {});
                        }

                        Button {
                            id: testSignalWithValue
                            text: "signalWithValue"
                            onClicked: databaseProxy.testSignal(function(error, result) {});
                        }
                    }

                    Row {
                        Button {
                            id: testEmptyInterfaceSlot
                            text: "callEmptyInterfaceSlot"
                            onClicked: emptyInterface.emptyAdaptorBaseSlot(function (error, result) {});
                        }

                        Button {
                            id: testEmptyInterfaceProperty
                            text: "getPropertyFromEmptyInterface"
                            onClicked: {
                                resultField.text = "read value: " + emptyInterface.emptyAdaptorBaseProp
                            }
                        }
                    }

                    Connections {
                        target: databaseProxy
                        onSignalWithValue: resultField.text = "got value: " + value;
                        onSignalWithNoValue: resultField.text = "signal without value called!"
                    }

                    Text { text: qsTr("Error") }
                    TextField { width: 500; id: errorField }

                    Text { text: qsTr("Result") }
                    TextArea { width: 500; height: 100; id: resultField }
                }
            }
        }
    }
}
