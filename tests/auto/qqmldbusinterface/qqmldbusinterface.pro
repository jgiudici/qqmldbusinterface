DEPTH = ../../..
include($${DEPTH}/qqmldbusinterface.pri)

TEMPLATE = app
TARGET = tst_qqmldbusinterface
INCLUDEPATH += $${QQMLDBUSINTERFACE_INCLUDEPATH}
LIBS += $${QQMLDBUSINTERFACE_LIBS}
QT += testlib dbus quick
CONFIG += testcase

HEADERS = \
    adaptor.h \
    helper.h
SOURCES = \
    adaptor.cpp \
    helper.cpp \
    tst_qqmldbusinterface.cpp
