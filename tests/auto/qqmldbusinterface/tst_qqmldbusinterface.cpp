#include <QtTest>
#include <QQmlEngine>
#include <QQmlContext>
#include <QQmlComponent>
#include <QQmlExpression>

#include "qqmldbusinterface.h"
#include "adaptor.h"
#include "helper.h"

class tst_QQmlDBusInterface : public QObject
{
    Q_OBJECT
private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();

    void readProperty_data();
    void readProperty();
    void writeProperty_data();
    void writeProperty();

    void methodWithArguments();
    void methodWithReturn();
    void methodWithReturnAndArguments();
    void methodThatReturnsAVariant();
    void methodThatReturnsAVariantMap();
    void methodThatReturnsAVariantMapOfVariantMaps();
    void methodThatTakesAndReturnsAVariantMap();

    void interfaceSignals_data();
    void interfaceSignals();

private:
    TestAdaptor *adaptor;
    QQmlDBusInterface *interface;
    QQmlEngine engine;
    QScopedPointer<QObject> rootObject;
    InterfaceHelper helper;

};

void tst_QQmlDBusInterface::initTestCase()
{
    // setup adaptor
    adaptor = new TestAdaptor(this);
    QDBusConnection sessionBus = QDBusConnection::sessionBus();
    QVERIFY(sessionBus.registerService("com.devonit.qqmldbusinterface"));
    QVERIFY(sessionBus.registerObject("/", this));

    // setup interface
    interface = new QQmlDBusInterface("com.devonit.qqmldbusinterface", "/",
                                      "com.devonit.qqmldbusinterface.test",
                                      QDBusConnection::sessionBus(), this);

    // NOTE: bad. dangerous. crazy. fix.
    //       what if its never valid, etc..
    while (!interface->isValid())
        QTestEventLoop::instance().enterLoopMSecs(20);
    QVERIFY(interface->isValid());

    engine.rootContext()->setContextProperty("iface", interface);
    engine.rootContext()->setContextProperty("helper", &helper);

    QQmlComponent component(&engine);
    component.setData("import QtQuick 2.0; QtObject {}", QUrl());
    rootObject.reset(component.create());
    QVERIFY(!rootObject.isNull());
}

void tst_QQmlDBusInterface::cleanupTestCase()
{
    adaptor->resetData();
    helper.reset();
}

void tst_QQmlDBusInterface::readProperty_data()
{
    QTest::addColumn<QString>("propertyName");
    QTest::addColumn<QVariant>("expected");

    QTest::newRow("string") << "stringProperty" << QVariant("string");
    QTest::newRow("int") << "intProperty" << QVariant(42);
    QTest::newRow("variant") << "variantProperty" << QVariant(42);

    {
        QStringList result = QStringList() << "surprise" << "i" << "am" << "jose" << "greco";
        QTest::newRow("stringList") << "stringListProperty" << QVariant(result);
    }

    {
        QVariantMap result;
        result.insert("test", "property");
        result.insert("another", "test");
        result.insert("number", 31337);
        QTest::newRow("variantMap") << "variantMapProperty" << QVariant(result);
    }

    {
        QVariantMap subSubVariantMap;
        subSubVariantMap.insert("hello", 42);
        subSubVariantMap.insert("dogs", "cats");

        QVariantMap subVariantMap;
        subVariantMap.insert("hello", 42);
        subVariantMap.insert("dogs", "cats");
        subVariantMap.insert("subSubVariantMap", subSubVariantMap);

        QVariantMap subVariantMap2;
        subVariantMap2.insert("hello", 42);
        subVariantMap2.insert("dogs", "cats");

        QVariantMap result;
        result.insert("hello", 42);
        result.insert("dogs", "cats");
        result.insert("subVariantMap", subVariantMap);
        result.insert("subVariantMap2", subVariantMap2);
        QTest::newRow("variantMapOfVariantMaps") << "variantMapOfVariantMapsProperty" << QVariant(result);
    }
}

void tst_QQmlDBusInterface::readProperty()
{
    QFETCH(QString, propertyName);
    QFETCH(QVariant, expected);

    QQmlExpression expression(engine.rootContext(), rootObject.data(), "iface." + propertyName);
    QVariant result = expression.evaluate();
    QVERIFY(!expression.hasError());
    QCOMPARE(result, expected);
}

void tst_QQmlDBusInterface::writeProperty_data()
{
    QTest::addColumn<QString>("propertyName");
    QTest::addColumn<QString>("data");
    QTest::addColumn<QVariant>("expected");

    QTest::newRow("string") << "stringProperty" << "'newstring'" << QVariant("newstring");
    QTest::newRow("int") << "intProperty" << "99" << QVariant(99);

    {
        // no need for variant map representation beccause we are checking the adaptor
        // not JSValues
        QStringList expected = QStringList() << "dogs" << "cats" << "monkies";
        QString data = "['dogs', 'cats', 'monkies']";
        QTest::newRow("stringList") << "stringListProperty" << data << QVariant(expected);
    }

    {
        QDBusVariant result(13.37);
        QVariant variant;
        variant.setValue(result);
        QTest::newRow("variant") << "variantProperty" << "13.37" << variant;
    }

    {
        QVariantMap expected;
        expected.insert("dogs", "rule");
        expected.insert("cats", "drool");
        QString data = "{dogs: 'rule', cats: 'drool'}";
        QTest::newRow("variantMap") << "variantMapProperty" << data << QVariant(expected);
    }

    /*
     * NOTE: I don't actually think this is possible, it will always end up on the Qt adaptor
     *       side as a QVariantMap full of QDBusArguments, and you need to convert it THERE,
     *       which we can't guarantee from this library
     *
    {
        QVariantMap subSubVariantMap;
        subSubVariantMap.insert("dogs", "rule");
        subSubVariantMap.insert("cats", "drool");

        QVariantMap subVariantMap;
        subVariantMap.insert("wolf", 42);
        subVariantMap.insert("elephant", 23);
        subVariantMap.insert("subsub", subSubVariantMap);

        QVariantMap subVariantMap2;
        subVariantMap2.insert("beatles", false);
        subVariantMap2.insert("giraffe", true);

        QVariantMap expected;
        expected.insert("sub", subVariantMap);
        expected.insert("othersub", subVariantMap2);

        QString data = "{"
        "    \"sub\": {"
        "        \"wolf\": 42,"
        "        \"elephant\": 23,"
        "        \"subsub\": {"
        "            \"dogs\": \"rule\","
        "            \"cats\": \"drool\""
        "        }"
        "    },"
        "    \"othersub\": {"
        "        \"beatles\": false,"
        "        \"giraffe\": true"
        "    }"
        "}";

        QTest::newRow("variantMapOfVariantMaps") << "variantMapOfVariantMapsProperty" << data << QVariant(expected);
    }
    */
}

void tst_QQmlDBusInterface::writeProperty()
{
    QFETCH(QString, propertyName);
    QFETCH(QString, data);
    QFETCH(QVariant, expected);

    QQmlExpression expression(engine.rootContext(), rootObject.data(),
                              "iface." + propertyName + " = " + data);
    expression.evaluate();
    QVERIFY(!expression.hasError());

    QVariant actual = adaptor->property(propertyName.toLatin1());
    if (actual.userType() == qMetaTypeId<QDBusVariant>())
        actual = actual.value<QDBusVariant>().variant();
    if (expected.userType() == qMetaTypeId<QDBusVariant>())
        expected = expected.value<QDBusVariant>().variant();
    QCOMPARE(actual, expected);
}

void tst_QQmlDBusInterface::methodWithArguments()
{
    QQmlExpression expression(engine.rootContext(), rootObject.data(),
                              "iface.methodWithArguments('test', 42, helper.callback)");
    expression.evaluate();
    QVERIFY(!expression.hasError());
    QVERIFY(helper.waitForResult());
    QVERIFY(!helper.isError());
}

void tst_QQmlDBusInterface::methodWithReturn()
{
    QQmlExpression expression(engine.rootContext(), rootObject.data(),
                              "iface.methodWithReturn(helper.callback)");
    expression.evaluate();
    QVERIFY(!expression.hasError());
    QVERIFY(helper.waitForResult());
    QVERIFY(!helper.isError());
    QCOMPARE(helper.result().toBool(), true);
}

void tst_QQmlDBusInterface::methodWithReturnAndArguments()
{
    QQmlExpression expression(engine.rootContext(), rootObject.data(),
                              "iface.methodWithReturnAndArguments('hello', helper.callback)");
    expression.evaluate();
    QVERIFY(!expression.hasError());
    QVERIFY(helper.waitForResult());
    QVERIFY(!helper.isError());
    QCOMPARE(helper.result().toString(), QLatin1String("hello world!"));
}

void tst_QQmlDBusInterface::methodThatReturnsAVariant()
{
    QQmlExpression expression(engine.rootContext(), rootObject.data(),
                              "iface.methodThatReturnsAVariant(helper.callback)");
    expression.evaluate();
    QVERIFY(!expression.hasError());
    QVERIFY(helper.waitForResult());
    QVERIFY(!helper.isError());
    QCOMPARE(helper.result().toInt(), 42);
}

void tst_QQmlDBusInterface::methodThatReturnsAVariantMap()
{
    QVariantMap expected;
    expected.insert("hello", 42);
    expected.insert("dogs", "cats");

    QQmlExpression expression(engine.rootContext(), rootObject.data(),
                              "iface.methodThatReturnsAVariantMap(helper.callback)");
    expression.evaluate();
    QVERIFY(!expression.hasError());
    QVERIFY(helper.waitForResult());
    QVERIFY(!helper.isError());
    QCOMPARE(helper.result().toMap(), expected);
}

void tst_QQmlDBusInterface::methodThatReturnsAVariantMapOfVariantMaps()
{
    QVariantMap subSubVariantMap;
    subSubVariantMap.insert("hello", 42);
    subSubVariantMap.insert("dogs", "cats");

    QVariantMap subVariantMap;
    subVariantMap.insert("hello", 42);
    subVariantMap.insert("dogs", "cats");
    subVariantMap.insert("subSubVariantMap", subSubVariantMap);

    QVariantMap subVariantMap2;
    subVariantMap2.insert("hello", 42);
    subVariantMap2.insert("dogs", "cats");

    QVariantMap expected;
    expected.insert("hello", 42);
    expected.insert("dogs", "cats");
    expected.insert("subVariantMap", subVariantMap);
    expected.insert("subVariantMap2", subVariantMap2);

    QQmlExpression expression(engine.rootContext(), rootObject.data(),
                              "iface.methodThatReturnsAVariantMapOfVariantMaps(helper.callback)");
    expression.evaluate();
    QVERIFY(!expression.hasError());
    QVERIFY(helper.waitForResult());
    QVERIFY(!helper.isError());
    QCOMPARE(helper.result().toMap(), expected);
}

void tst_QQmlDBusInterface::methodThatTakesAndReturnsAVariantMap()
{
    QVariantMap expected;
    expected.insert("dogs", "rule");
    expected.insert("cats", "drool");

    QString data = "{'dogs': 'rule', 'cats': 'drool'}";
    QQmlExpression expression(engine.rootContext(), rootObject.data(),
                              "iface.methodThatTakesAndReturnsAVariantMap(" + data + ", helper.callback);");
    expression.evaluate();
    QVERIFY(!expression.hasError());
    QVERIFY(helper.waitForResult());
    QVERIFY(!helper.isError());
    QCOMPARE(helper.result().toMap(), expected);
}

void tst_QQmlDBusInterface::interfaceSignals_data()
{
    QTest::addColumn<QByteArray>("signalName");
    QTest::addColumn<QByteArray>("parameter");
    QTest::addColumn<QVariant>("expected");

    QTest::newRow("novalue")
        << QByteArray("WithNoValue")
        << QByteArray("")
        << QVariant();

    QTest::newRow("string")
        << QByteArray("WithStringValue")
        << QByteArray("string")
        << QVariant("string");

    {
        QVariant variant;
        variant.setValue(QDBusVariant(11.22));
        QTest::newRow("variant")
            << QByteArray("WithVariantValue")
            << QByteArray("variant")
            << variant;
    }

    {
        QVariantMap variantMap;
        variantMap.insert("donkeys", "will");
        variantMap.insert("rule", "the");
        variantMap.insert("modern", "world");

        QTest::newRow("variantmap")
            << QByteArray("WithVariantMapValue")
            << QByteArray("variantMap")
            << QVariant(variantMap);
    }
}

void tst_QQmlDBusInterface::interfaceSignals()
{
    QFETCH(QByteArray, signalName);
    QFETCH(QByteArray, parameter);
    QFETCH(QVariant, expected);

    QByteArray slotName = "slot" + signalName;
    QByteArray emitCommand = "emitSignal" + signalName;

    QByteArray handler =
        QString("onSignal%1: helper.%2(%3);")
            .arg(signalName.constData())
            .arg(slotName.constData())
            .arg(parameter.constData())
            .toUtf8();

    QByteArray data =  \
        "import QtQuick 2.0;"
        "Item { "
        "    function emptyCallback(error, result) { } "
        "    Connections { "
        "        target: iface; "
        "        " + handler + ""
        "    } "
        "}";

    QQmlComponent component(&engine);
    component.setData(data, QUrl());
    QObject *object = component.create();
    QVERIFY(object);

    QString command = QString("iface.%1(emptyCallback);").arg(emitCommand.constData());
    QQmlExpression expression(engine.rootContext(), object, command);
    expression.evaluate();
    QVERIFY(!expression.hasError());
    QVERIFY(helper.waitForResult());
    QCOMPARE(helper.result(), expected);
}

/*
void signalWithNoValue();
void signalWithStringValue(const QString &value);
void signalWithVariantValue(const QDBusVariant &value);
void signalWithVariantMapValue(const QVariantMap &value);
*/

/*
void slotWithNoValue();
void slotWithStringValue(const QString &string);
void slotWithVariantValue(const QDBusVariant &variant);
void slotWithVariantMapValue(const QVariantMap &map);
*/
QTEST_GUILESS_MAIN(tst_QQmlDBusInterface)
#include "tst_qqmldbusinterface.moc"
